export const environment = {
    version: require('../../package.json').version,
    production: true,
    servers: [
        {
            name: 'scripts_server',
            url: ''
        },
        {
            name: 'send_server',
            url: ''
        },
        {
            name: 'image_server',
            url: '',
            dir: ''
        }
    ],
    skipRegisterSMS: false,
    netPackageDebug: false,
    dataDebug: false,
    encodingDebug: false
};
