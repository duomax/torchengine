import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomCheckedButtonComponent } from './custom-checked-button.component';

describe('CustomCheckedButtonComponent', () => {
  let component: CustomCheckedButtonComponent;
  let fixture: ComponentFixture<CustomCheckedButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomCheckedButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomCheckedButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
