import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ButtonState, CheckedButtonState} from "../buttons";
import {States} from "../../states";

@Component({
    selector: 'torch-custom-checked-button',
    templateUrl: './custom-checked-button.component.html',
    styleUrls: ['./custom-checked-button.component.css']
})
export class CustomCheckedButtonComponent implements OnInit {
    private _checked: boolean;
    @Input() get checked(): boolean {
        return this._checked;
    }
    @Output() checkedChange: EventEmitter<boolean>;
    set checked(v: boolean) {
        this._checked = v;
        this.state.setStates([
            { name: States.STATE_CHECKED, value: v }
        ]);
        console.log("ma3ks state", this.state);
        this.stateChange.emit(this.state);
        this.checkedChange.emit(v);
    }

    private _state: CheckedButtonState;
    @Input() get state(): CheckedButtonState {
        return this._state;
    }
    @Output() stateChange: EventEmitter<CheckedButtonState>;
    set state(v: CheckedButtonState) {
        this._state = v;
        this.stateChange.emit(v);
    }

    @Output() torchchecked: EventEmitter<boolean>;

    hasClickObservers: boolean;
    @Input() preferredStopPropagation: boolean;

    constructor() {
        this._state = new CheckedButtonState();
        this._checked = false;
        this.stateChange = new EventEmitter();
        this.checkedChange = new EventEmitter();
        this.torchchecked = new EventEmitter();

        this.hasClickObservers = false;
        this.preferredStopPropagation = null;
    }

    ngOnInit() {
        this.state.setStates([
            { name: States.STATE_CHECKED, value: this.checked }
        ]);

        this.hasClickObservers = null;//this.torchchecked.observers.length > 0 || this.checkedChange.observers.length > 0 || this.stateChange.observers.length > 0;
    }

    select() {
        this.checked = !this.checked;
        this.torchchecked.emit(this.checked);
    }

}
