import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { ButtonState, ButtonTypes } from "../buttons";
import { States } from "../../states";

@Component({
    selector: 'torch-custom-button',
    templateUrl: './custom-button.component.html',
    styleUrls: ['./custom-button.component.css']
})
export class CustomButtonComponent implements OnInit, OnDestroy {
    private _state: ButtonState;
    @Input() get state(): ButtonState {
        return this._state;
    }
    @Output() stateChange: EventEmitter<ButtonState>;
    set state(v: ButtonState) {
        this._state = v;
        this.stateChange.emit(v);
    }

    @Input() type: string;

    @Output() torchclick: EventEmitter<Event>;

    private _views: any;

    initViews: boolean;

    unsubsribers: (() => void)[];

    hasClickObservers: boolean;
    @Input() externalObservers: boolean;
    @Input() preferredStopPropagation: boolean;

    constructor(public element: ElementRef) {
        this.initViews = false;

        this._state = new ButtonState();
        this._views = [];

        this.type = ButtonTypes.BUTTON_TYPE_BUTTON;

        this.torchclick = new EventEmitter();

        this.unsubsribers = [];

        this.hasClickObservers = false;
        this.externalObservers = null;
        this.preferredStopPropagation = null;

        this.stateChange = new EventEmitter();
    }

    ngOnInit() {
        this.hasClickObservers = (this.externalObservers != null) ? this.externalObservers : this.torchclick.observers.length > 0;

        setTimeout(() => {
            let disabledView: HTMLElement = this.element.nativeElement.querySelector(".disabled");
            let hoveredView: HTMLElement = this.element.nativeElement.querySelector(".hovered");
            let pressedView: HTMLElement = this.element.nativeElement.querySelector(".pressed");

            this._views = {
                disabled: disabledView != null/* && disabledView.children.length != 0*/,
                hovered: hoveredView != null/* && hoveredView.children.length != 0*/,
                pressed: pressedView != null/* && pressedView.children.length != 0*/,
            };

            this.initViews = true;
        }, 0);
    }

    ngOnDestroy() {
        this.unsubsribers.forEach((unsubscriber: () => void) => unsubscriber());
    }

    showContentView(): boolean {
        return true; // !this.isHovered() && !this.isDisabled() && !this.isPressed() || !this.initViews;
    }

    showHoveredView(): boolean {
        return this.isHovered() && !this.state.disabled && !this.isPressed() || !this.initViews
    }

    showPressedView(): boolean {
        return this.isPressed() && !this.state.disabled || !this.initViews;
    }

    showDisabledView(): boolean {
        return this.isDisabled() || !this.initViews;
    }

    private isDisabled(): boolean {
        return this.state.disabled && this._views.disabled && this.isOnlyState("disabled");
    }

    private isPressed(): boolean {
        return this.state.pressed && this._views.pressed && this.isOnlyState("pressed");
    }

    private isHovered(): boolean {
        return this.state.hovered && this._views.hovered && this.isOnlyState("hovered");
    }

    private isOnlyState(stateName: string) {
        let onlyState: boolean = true;
        this.state.getStates().forEach((value: boolean, key: string) => {
            if (key != stateName && value == true)
                onlyState = false;
        });

        return true;
    }

    onMouseOver(event: Event): void {
        this.setState(States.STATE_HOVERED, !this.state.disabled);
    }

    onMouseOut(event: Event): void {
        this.setState(States.STATE_HOVERED, false);
    }

    onMouseDown(event) {
        this.setState(States.STATE_PRESSED, !this.state.disabled);
    }
    onMouseUp(event) {
        if (!this.state.disabled)
            this.setState(States.STATE_PRESSED, false);
    }

    private setState(name: string, value: boolean): void {
        this.state.setStates([{ name: name, value: value }]);
        this.stateChange.emit(this.state);
    }

    onClick(event: Event): void {
        if (!this.state.disabled) {
            this.element.nativeElement.querySelector("#native-btn").click();
            this.torchclick.emit(event);
        }
    }

    onNativeClick(event: Event): void {
        event.stopPropagation(); // Prevent firing all parents events
    }
}
