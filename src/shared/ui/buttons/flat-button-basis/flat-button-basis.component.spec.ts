import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlatButtonBasisComponent } from './flat-button-basis.component';

describe('FlatButtonBasisComponent', () => {
  let component: FlatButtonBasisComponent;
  let fixture: ComponentFixture<FlatButtonBasisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlatButtonBasisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlatButtonBasisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
