import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Color, IColor} from "../../colors";
import {DebugService} from "../../../../core/debug.service";
import {States} from "../../states";

@Component({
    selector: 'torch-flat-button-basis',
    templateUrl: './flat-button-basis.component.html',
    styleUrls: ['./flat-button-basis.component.css']
})
export class FlatButtonBasisComponent implements OnInit {
    @Input() buttonType: number;
    @Input() controls: any[];
    @Input() caption: string;
    @Input() captionAlign: string;
    @Input() borderRadius: number;
    @Input() colors: Map<string, IColor>;
    @Input() externalObservers: boolean;

    defaultColorSchema: Map<string, IColor>;

    private _checked: boolean;
    @Input() get checked(): boolean {
        return this._checked;
    }
    @Output() checkedChange: EventEmitter<boolean>;
    set checked(v: boolean) {
        this._checked = v;
        this.checkedChange.emit(v);
    }

    @Output() torchclick: EventEmitter<Event>;
    @Output() torchchecked: EventEmitter<Event>;

    hasClickObservers: boolean;
    // @Input() preferredStopPropagation: boolean;

    constructor(private debugService: DebugService) {
        this.buttonType = 0; // 0 - usual button, 1 - checked button

        // this.colors = [];

        this.caption = "";
        this.borderRadius = 0;

        this.colors = new Map();
        this.defaultColorSchema = Color.COLOR_SCHEMAS.get(Color.COLOR_SCHEMA_GREY);

        this._checked = false;
        this.checkedChange = new EventEmitter();

        this.torchclick = new EventEmitter();
        this.torchchecked = new EventEmitter();

        this.externalObservers = null;
    }

    ngOnInit() {
        // console.group("%cFlatButtonBasis", "color:yellow");
        let warningMessages: string[] = [];
        if (!this.colors) {
            warningMessages.push("FlatButtonBasis: Input variable colors is null");
            // console.warn("FlatButtonBasis: Input variable colors is null");
            this.colors = new Map();
        }

        if (this.caption == null) {
            warningMessages.push("FlatButtonBasis: Input variable caption is null");
            // console.warn("FlatButtonBasis: Input variable caption is null");
            this.caption = "";
        }

        if (this.buttonType == null) {
            warningMessages.push("FlatButtonBasis: Input variable buttonType is null");
            // console.warn("FlatButtonBasis: Input variable buttonType is null");
            this.buttonType = 0;
        }

        DebugService.warningGroup("FlatButtonBasis", warningMessages);

        // if (warningMessages.length > 0) {
        //     console.group("%cFlatButtonBasis", "background:yellow; display: block");
        //     console.log('%c a colorful message', 'background: green; color: white; display: block;');
        //     warningMessages.forEach((message: string) => console.warn(message));
        //     console.groupEnd();
        // }

        // console.assert(this.caption, "FlatButtonBasis: Input variable caption is null");
        // console.table("ma3ks");

        // console.groupEnd();

        // let stack = new Error().stack;
        // console.log("PRINTING CALL STACK");
        // console.log( stack );
        // console.group("111");
        // console.log("222");
        // console.groupEnd();
        // console.profile("333");
        // console.log("444");
        // console.profileEnd();
        // console.trace("555");

        this.defaultColorSchema.forEach((value: IColor, key: string) => {
            if (!this.colors.get(key)) this.colors.set(key, value);
        });
    }

    onClick(event: Event): void {
        this.torchclick.emit(event);
    }

    onCheck(event: Event): void {
        this.torchchecked.emit(event);
    }

    getCommonColor(): string {
        return Color.numberToCssColor(this.getColor(States.STATE_COMMON).color);
    }

    getCommonBackgroundColor(): string {
        return Color.numberToCssColor(this.getColor(States.STATE_COMMON).background_color);
    }

    getCommonBorderColor(): string {
        return Color.numberToCssColor(this.getColor(States.STATE_COMMON).border_color || -1);
    }

    getHoveredColor(): string {
        return Color.numberToCssColor(this.getColor(States.STATE_HOVERED).color);
    }

    getHoveredBackgroundColor(): string {
        return Color.numberToCssColor(this.getColor(States.STATE_HOVERED).background_color);
    }

    getHoveredBorderColor(): string {
        return Color.numberToCssColor(this.getColor(States.STATE_HOVERED).border_color || -1);
    }

    getPressedColor(): string {
        return Color.numberToCssColor(this.getColor(States.STATE_PRESSED).color);
    }

    getPressedBackgroundColor(): string {
        return Color.numberToCssColor(this.getColor(States.STATE_PRESSED).background_color);
    }

    getPressedBorderColor(): string {
        return Color.numberToCssColor(this.getColor(States.STATE_PRESSED).border_color || -1);
    }

    getDisabledColor(): string {
        return Color.numberToCssColor(this.getColor(States.STATE_DISABLED).color);
    }

    getDisabledBackgroundColor(): string {
        return Color.numberToCssColor(this.getColor(States.STATE_DISABLED).background_color);
    }

    getDisabledBorderColor(): string {
        return Color.numberToCssColor(this.getColor(States.STATE_DISABLED).border_color || -1);
    }

    getCheckedColor(): string {
        let color = (this.getColor(States.STATE_CHECKED)) ? this.getColor(States.STATE_CHECKED) : this.getColor(States.STATE_PRESSED);
        return Color.numberToCssColor(color.color);
    }

    getCheckedBackgroundColor(): string {
        let color = (this.getColor(States.STATE_CHECKED)) ? this.getColor(States.STATE_CHECKED) : this.getColor(States.STATE_PRESSED);
        return Color.numberToCssColor(color.background_color);
    }

    getCheckedBorderColor(): string {
        let color = (this.getColor(States.STATE_CHECKED)) ? this.getColor(States.STATE_CHECKED) : this.getColor(States.STATE_PRESSED);
        return Color.numberToCssColor(color.border_color || -1);
    }

    private getColor(schemaName: string): IColor {
        return this.colors.get(schemaName);
    }
}
