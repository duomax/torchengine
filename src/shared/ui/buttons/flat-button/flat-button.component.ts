import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ButtonAlign, ButtonState } from "../buttons";
import { States } from "../../states";
import { IColor } from "../../colors";

@Component({
    selector: 'torch-flat-button',
    templateUrl: './flat-button.component.html',
    styleUrls: ['./flat-button.component.css']
})
export class FlatButtonComponent implements OnInit {
    @Input() caption: string;
    @Input() captionAlign: string;
    @Input() colors: Map<string, IColor>;
    @Input() borderRadius: number;
    @Input() externalObservers: boolean;

    @Output() disabledChange: EventEmitter<boolean>;
    @Input() get disabled(): boolean {
        return this.state.disabled;
    };
    set disabled(v: boolean) {
        this.state.setStates([{ name: States.STATE_DISABLED, value: v }], true);
        this.disabledChange.emit(v);
    }

    @Output() torchclick: EventEmitter<Event>;

    state: ButtonState;
    hasObservers: boolean;

    constructor() {
        this.caption = "";
        this.state = new ButtonState();

        this.disabledChange = new EventEmitter(false);
        this.torchclick = new EventEmitter();

        this.disabled = false;

        this.borderRadius = 0;
        this.captionAlign = ButtonAlign.BUTTON_ALIGN_CENTER;

        this.colors = new Map();

        this.hasObservers = false;
        this.externalObservers = null;
    }

    ngOnInit() {
        this.hasObservers = (this.externalObservers != null) ? this.externalObservers : this.torchclick.observers.length > 0;
    }

    onClick(event: Event): void {
        this.torchclick.emit(event);
    }
}
