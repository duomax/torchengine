import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlatCheckedButtonComponent } from './flat-checked-button.component';

describe('FlatCheckedButtonComponent', () => {
  let component: FlatCheckedButtonComponent;
  let fixture: ComponentFixture<FlatCheckedButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlatCheckedButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlatCheckedButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
