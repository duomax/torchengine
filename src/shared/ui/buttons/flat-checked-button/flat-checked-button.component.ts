import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FlatButtonComponent } from "../flat-button/flat-button.component";

@Component({
    selector: 'torch-flat-checked-button',
    templateUrl: './flat-checked-button.component.html',
    styleUrls: ['./flat-checked-button.component.css']
})
export class FlatCheckedButtonComponent extends FlatButtonComponent implements OnInit {
    private _checked: boolean;
    @Input() get checked(): boolean {
        return this._checked;
    }
    @Output() checkedChange: EventEmitter<boolean>;
    set checked(v: boolean) {
        this._checked = v;
        this.checkedChange.emit(v);
    }

    @Output() torchclick: EventEmitter<Event>;
    @Output() torchchecked: EventEmitter<Event>;

    constructor() {
        super();
        this._checked = false;
        this.checkedChange = new EventEmitter();

        this.torchclick = new EventEmitter();
        this.torchchecked = new EventEmitter();
    }

    ngOnInit() {
    }

    onClick(event: Event): void {
        this.torchclick.emit(event);
    }

    onCheck(event: Event): void {
        this.torchchecked.emit(event);
    }
}
