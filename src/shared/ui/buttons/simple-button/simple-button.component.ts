import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Color, IColor} from "../../colors";
import {States} from "../../states";

@Component({
    selector: 'torch-simple-button',
    templateUrl: './simple-button.component.html',
    styleUrls: ['./simple-button.component.css']
})
export class SimpleButtonComponent implements OnInit {
    @Input() caption: string;
    @Input() colorSchema: string;
    @Input() borderRadius: number;

    @Output() torchclick: EventEmitter<Event>;

    @Output() disabledChange: EventEmitter<boolean>;
    private _disabled: boolean;
    @Input() get disabled(): boolean {
        return this._disabled;
    };
    set disabled(v: boolean) {
        this._disabled = v;
        this.disabledChange.emit(v);
    }

    hasObservers: boolean;

    constructor() {
        this._disabled = false;

        this.caption = "";
        this.colorSchema = Color.COLOR_SCHEMA_GREY;

        this.borderRadius = 5;

        this.torchclick = new EventEmitter();
        this.disabledChange = new EventEmitter();

        this.hasObservers = false;
    }

    ngOnInit() {
        this.hasObservers = this.torchclick.observers.length > 0;
    }

    getColors(): Map<string, IColor> {
        return Color.COLOR_SCHEMAS.get(this.colorSchema);
    }

    onClick(event: Event): void {
        this.torchclick.emit(event);
    }
}
