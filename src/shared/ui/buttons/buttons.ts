import {InteractiveState} from "../Interactive";
import {IDisabled, IStates, States} from "../states";

// export interface IButtonState extends IState {
//     disabled: boolean
// }

// export type IButtonState = IState;

export class ButtonState extends InteractiveState {
    get disabled(): boolean {
        return this.getStates().get(States.STATE_DISABLED);
    }

    constructor() {
        super();
        this.setDefaultStates();
    }

    setDefaultStates(): void {
        this.setStates([
            { name: States.STATE_HOVERED, value: false },
            { name: States.STATE_DISABLED, value: false },
            { name: States.STATE_PRESSED, value: false }
        ]);
    }
}
export class CheckedButtonState extends ButtonState {
    get checked(): boolean {
        return this.getStates().get(States.STATE_CHECKED);
    }

    constructor() {
        super();
        this.setDefaultStates();
    }

    setDefaultStates(): void {
        this.setStates([
            { name: States.STATE_HOVERED, value: false },
            { name: States.STATE_DISABLED, value: false },
            { name: States.STATE_PRESSED, value: false },
            { name: States.STATE_CHECKED, value: false }
        ]);
    }
}


export class ButtonTypes {
    public static readonly BUTTON_TYPE_SUBMIT: string = "submit";
    public static readonly BUTTON_TYPE_BUTTON: string = "button";
    public static readonly BUTTON_TYPE_RESET: string = "reset";
}

export class ButtonAlign {
    public static readonly BUTTON_ALIGN_LEFT: string = "flex-start";
    public static readonly BUTTON_ALIGN_CENTER: string = "center";
    public static readonly BUTTON_ALIGN_RIGHT: string = "flex-end";
}