// export interface IState {
//     hovered: boolean,
//     pressed: boolean
// }

import {IHovered, IPressed, IState, IStates, States} from "./states";

export class InteractiveState {
    // public static readonly INTERACTIVE_STATE_HOVERED: string = "hovered";
    // public static readonly INTERACTIVE_STATE_PRESSED: string = "pressed";

    private _states: Map<string, boolean>;
    // readonly _states: IState[];

    get hovered(): boolean {
        // let states: IState = this._states.find((state: IState) => typeof(state) === IHovered);
        // return states.hovered | false;
        return this._states.get(States.STATE_HOVERED);
    }
    get pressed(): boolean {
        // let states: IState = this._states.find((state: IState) =>  typeof(state) === IPressed);
        // return states.pressed | false;
        return this._states.get(States.STATE_PRESSED);
    }

    constructor() {
        this._states = new Map();
        // this._states = new IState();
        this.setDefaultStates();
    }

    setStates(states: any[], setToDefault: boolean = false): void {
        if (setToDefault)
            this.setDefaultStates();

        // this._states = states;
        // states.forEach((state: any) => {
        //     state.keys().forEach((key: string) => {
                // this._states[key] = state[key];
            // });
        // });
        states.forEach((state: any) => {
            this._states.set(state.name, state.value);
        });
    }

    getStates(): Map<string, boolean> {
        return this._states;
    }
    // getStates(): IStates {
    //     return this._states;
    // }

    setDefaultStates(): void {
        this.setStates([
            // { hovered: false },
            // { pressed: false }
            { name: States.STATE_HOVERED, value: false },
            { name: States.STATE_PRESSED, value: false }
        ]);
    }
}