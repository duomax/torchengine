import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import { WindowService } from "../../../core/window.service";

@Component({
    selector: 'torch-interactive',
    templateUrl: './interactive.component.html',
    styleUrls: ['./interactive.component.css']
})
export class InteractiveComponent implements OnInit, OnDestroy, AfterViewInit {
    @Output() torchmouseover: EventEmitter<Event>;
    @Output() torchmouseout: EventEmitter<Event>;
    @Output() torchmousedown: EventEmitter<Event>;
    @Output() torchmouseup: EventEmitter<Event>;
    @Output() torchclick: EventEmitter<Event>;

    @Input() externalObservers: boolean;
    @Input() preferredStopPropagation: boolean;

    unsubscribers: (() => void)[];

    constructor(private element: ElementRef, private windowService: WindowService) {
        this.torchmouseover = new EventEmitter();
        this.torchmouseout = new EventEmitter();
        this.torchmousedown = new EventEmitter();
        this.torchmouseup = new EventEmitter();
        this.torchclick = new EventEmitter();

        this.externalObservers = null;
        this.preferredStopPropagation = null;


        this.unsubscribers = [];
    }

    ngOnInit() {}

    ngAfterViewInit() {
        this.registerListener([this.torchmouseover, this.torchmouseout], WindowService.WINDOW_EVENT_MOUSE_OVER);
        this.registerListener([this.torchmousedown], WindowService.WINDOW_EVENT_MOUSE_DOWN);
        this.registerListener([this.torchmouseup], WindowService.WINDOW_EVENT_MOUSE_UP);
        this.registerListener([this.torchclick], WindowService.WINDOW_EVENT_CLICK);
    }

    ngOnDestroy() {
        this.unsubscribers.forEach((unsubscriber: () => void) => unsubscriber());
    }

    private registerListener(emiters: EventEmitter<Event>[], eventType: string): void {
        let hasObservers: boolean = (eventType == WindowService.WINDOW_EVENT_CLICK && this.externalObservers != null) ?
            this.externalObservers : emiters.filter((emiter: EventEmitter<Event>) => emiter.observers.length == 0).length == 0;

        if (hasObservers)
            this.unsubscribers.push(this.windowService.addEventListener(eventType, this.getHandler(eventType)));
    }

    private registerMouseOverListener(event: Event): boolean {
        if (this.element.nativeElement.contains(event.target))
            this.torchmouseover.emit(event);
        else
            this.torchmouseout.emit(event);

        return false; // TODO: Decide need to propagation this event or not
    }

    private registerMouseDownListener(event: MouseEvent): boolean  {
        if (this.element.nativeElement.contains(event.target) && event.which == 1)
            this.torchmousedown.emit(event);

        return false; // TODO: Decide need to propagation this event or not
    }

    private registerMouseUpListener(event: MouseEvent): boolean  {
        if (event.which == 1)
            this.torchmouseup.emit(event);

        return false; // TODO: Decide need to propagation this event or not
    }

    private registerClickListener(event: Event): boolean {
        if (this.element.nativeElement.contains(event.target)) {
            this.torchclick.emit(event);
            return (this.preferredStopPropagation != null) ? this.preferredStopPropagation : true;
        }

        return false;
    }

    private getHandler(eventType: string): (event: Event) => boolean {
        switch (eventType) {
            case WindowService.WINDOW_EVENT_MOUSE_OVER : return this.registerMouseOverListener.bind(this);
            case WindowService.WINDOW_EVENT_MOUSE_DOWN: return this.registerMouseDownListener.bind(this);
            case WindowService.WINDOW_EVENT_MOUSE_UP: return this.registerMouseUpListener.bind(this);
            default: return this.registerClickListener.bind(this);
        }
    }
}
