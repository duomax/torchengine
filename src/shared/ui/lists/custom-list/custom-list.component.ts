 import { Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'torch-custom-list',
    templateUrl: './custom-list.component.html',
    styleUrls: ['./custom-list.component.css']
})
export class CustomListComponent implements OnInit {
    @Output() selectedChange: EventEmitter<number>;
    private _selected: number;
    @Input() get selected(): number {
        return this._selected;
    }
    set selected(v: number) {
        this._selected = v;
        this.selectedChange.emit(v);
    }

    @Output() torchselect: EventEmitter<number>;

    @HostListener("click", ['$event']) onClick(event) {
        let list: NodeList = this.elementRef.nativeElement.querySelectorAll(".item");
        let listArray: Array<HTMLElement> = Array.prototype.slice.call(list); // Convert NodeList to Array

        let index: number = listArray.findIndex(item => item.contains(event.target));

        if (index >= 0) {
            this.selected = (index == this.selected) ? -1 : index;
        }

        this.torchselect.emit(this.selected);
    }

    constructor(private elementRef: ElementRef) {
        this._selected = -1;

        this.selectedChange = new EventEmitter();
        this.torchselect = new EventEmitter();
    }

    ngOnInit() {}
}
