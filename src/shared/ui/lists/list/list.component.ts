import {Component, ElementRef, OnInit} from '@angular/core';

@Component({
    selector: 'torch-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
    items: HTMLElement[];
    elem: HTMLElement;

    show: boolean = false;

    constructor(private element: ElementRef) {
        this.items = [];
    }

    ngOnInit() {
        setTimeout(() => {
            this.items = this.element.nativeElement.querySelectorAll(".item");
            this.show = true;
            console.log("ma3ks list", this.items);
        }, 0);
    }

    notEmpty() {
        return this.items.length == 0;
    }

}
