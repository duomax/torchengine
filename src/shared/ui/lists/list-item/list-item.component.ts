import {Component, ElementRef, Input, OnInit} from '@angular/core';

@Component({
    selector: 'torch-list-item',
    templateUrl: './list-item.component.html',
    styleUrls: ['./list-item.component.css']
})
export class ListItemComponent implements OnInit {
    @Input() item: HTMLElement;

    constructor(public element: ElementRef) {
        this.item = null;
    }

    ngOnInit() {}

    getElement(viewName: string): string {
        if (!this.item) return '';
        console.log("ma3ks get element", this.element.nativeElement.querySelector("." + viewName));
        let view = this.element.nativeElement.querySelector("." + viewName);
        return (view) ? view.innerHTML : '';
    }

    aa() {
        console.log("ma3ks aaa", this.element.nativeElement.parentElement.tagName);
        return this.element.nativeElement.parentElement.parentElement.tagName.toLowerCase() == "torch-list";
    }

}
