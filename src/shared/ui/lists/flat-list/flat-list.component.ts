import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { Color, IColor } from "../../colors";
import { ButtonAlign } from "../../buttons/buttons";
import {States} from "../../states";

@Component({
    selector: 'torch-flat-list',
    templateUrl: './flat-list.component.html',
    styleUrls: ['./flat-list.component.css']
})
export class FlatListComponent implements OnInit {
    @Input() colors: Map<string, IColor>;
    @Input() containerColors: Map<string, IColor>;
    @Input() items: string[];
    @Input() borderRadius: number;

    defaultColorSchema: Map<string, IColor>;
    defaultContainerColorSchema: Map<string, IColor>;
    captionAlign: string;

    @Output() selectedChange: EventEmitter<number>;
    private _selected: number;
    @Input() get selected(): number {
        return this._selected;
    }
    set selected(v: number) {
        this._selected = v;
        this.selectedChange.emit(v);
    }

    preventCheckedFunction: boolean;

    constructor() {
        this.items = [];

        this._selected = -1;

        this.captionAlign = ButtonAlign.BUTTON_ALIGN_LEFT;
        this.borderRadius = 5;

        this.colors = new Map();
        this.containerColors = new Map();
        this.defaultColorSchema = Color.COLOR_SCHEMAS.get(Color.COLOR_SCHEMA_WHITE);
        this.defaultContainerColorSchema = Color.COLOR_SCHEMAS.get(Color.COLOR_SCHEMA_GREY_BASIC);

        this.preventCheckedFunction = false;

        this.selectedChange = new EventEmitter();
    }

    ngOnInit() {
        this.defaultColorSchema.forEach((value: IColor, key: string) => {
            if (!this.colors.get(key)) this.colors.set(key, value);
        });
        this.defaultContainerColorSchema.forEach((value: IColor, key: string) => {
            if (!this.containerColors.get(key)) this.containerColors.set(key, value);
        });
    }

    onCheck(event: boolean, index: number): void {
        this.preventCheckedFunction = false;
    }

    isChecked(index: number): boolean {
        return (this.selected != index) ? false : (!this.preventCheckedFunction) ? true : null;
    }

    getBackgroundColor() {
        return Color.numberToCssColor(this.containerColors.get(States.STATE_COMMON).background_color);
    }

    getBorderColor() {
        return Color.numberToCssColor(this.containerColors.get(States.STATE_COMMON).border_color);
    }

    getBorder() {
        return (this.containerColors.get(States.STATE_COMMON).border_color) ? "1px solid" : "none";
    }

    onMouseDown() {
        this.preventCheckedFunction = true;
    }
}
