import {States} from "./states";

export interface IColor {
    color?: number,
    background_color?: number,
    border_color?: number
}

export class Color {
    // public static readonly COLOR_COMMON: string = "color_common";
    // public static readonly COLOR_HOVERED: string = "color_hovered";
    // public static readonly COLOR_PRESSED: string = "color_pressed";
    // public static readonly COLOR_DISABLED: string = "color_disabled";
    // public static readonly COLOR_CHECKED: string = "color_checked";

    public static readonly COLOR_SCHEMA_WHITE: string = "color_schema_white";
    public static readonly COLOR_SCHEMA_GREY: string = "color_schema_grey";
    public static readonly COLOR_SCHEMA_GREEN: string = "color_schema_green";
    public static readonly COLOR_SCHEMA_RED: string = "color_schema_red";
    public static readonly COLOR_SCHEMA_BLUE: string = "color_schema_blue";
    public static readonly COLOR_SCHEMA_YELLOW: string = "color_schema_yellow";
    public static readonly COLOR_SCHEMA_OLIVE: string = "color_schema_olive";

    public static readonly COLOR_SCHEMA_GREY_BASIC: string = "color_schema_grey_basic";

    public static readonly COLOR_SCHEMAS: Map<string, Map<string, IColor>> = new Map([
        [
            Color.COLOR_SCHEMA_WHITE,
            new Map([
                [States.STATE_COMMON, { color: 0x5a5a5a, background_color: 0xffffff } as IColor],
                [States.STATE_HOVERED, { color: 0x282929, background_color: 0xf7f7f7 } as IColor],
                [States.STATE_PRESSED, { color: 0x121212, background_color: 0xcacbcd } as IColor],
                [States.STATE_DISABLED, { color: 0xb5b5b5, background_color: 0xf1f1f2 } as IColor],
                [States.STATE_CHECKED, { color: 0x121212, background_color: 0xcacbcd } as IColor],
            ])
        ],
        [
            Color.COLOR_SCHEMA_GREY,
            new Map([
                [States.STATE_COMMON, { color: 0x5a5a5a, background_color: 0xe0e1e2 } as IColor],
                [States.STATE_HOVERED, { color: 0x282929, background_color: 0xcacbcd } as IColor],
                [States.STATE_PRESSED, { color: 0x121212, background_color: 0xbabbbc } as IColor],
                [States.STATE_DISABLED, { color: 0xb5b5b5, background_color: 0xf1f1f2 } as IColor],
                [States.STATE_CHECKED, { color: 0x121212, background_color: 0xbabbbc } as IColor],
            ])
        ],
        [
            Color.COLOR_SCHEMA_GREEN,
            new Map([
                [States.STATE_COMMON, { color: 0xffffff, background_color: 0x21ba45 } as IColor],
                [States.STATE_HOVERED, { color: 0xffffff, background_color: 0x16ab39 } as IColor],
                [States.STATE_PRESSED, { color: 0xffffff, background_color: 0x198f35 } as IColor],
                [States.STATE_DISABLED, { color: 0xffffff, background_color: 0x9ce0ac } as IColor],
                [States.STATE_CHECKED, { color: 0xffffff, background_color: 0x198f35 } as IColor],
            ])
        ],
        [
            Color.COLOR_SCHEMA_RED,
            new Map([
                [States.STATE_COMMON, { color: 0xffffff, background_color: 0xdb2828 } as IColor],
                [States.STATE_HOVERED, { color: 0xffffff, background_color: 0xd01919 } as IColor],
                [States.STATE_PRESSED, { color: 0xffffff, background_color: 0xb21e1e } as IColor],
                [States.STATE_DISABLED, { color: 0xffffff, background_color: 0xdd9a9a } as IColor],
                [States.STATE_CHECKED, { color: 0xffffff, background_color: 0xb21e1e } as IColor],
            ])
        ],
        [
            Color.COLOR_SCHEMA_BLUE,
            new Map([
                [States.STATE_COMMON, { color: 0xffffff, background_color: 0x2185d0 } as IColor],
                [States.STATE_HOVERED, { color: 0xffffff, background_color: 0x1678c2 } as IColor],
                [States.STATE_PRESSED, { color: 0xffffff, background_color: 0x1a69a4 } as IColor],
                [States.STATE_DISABLED, { color: 0xffffff, background_color: 0x99bcd6 } as IColor],
                [States.STATE_CHECKED, { color: 0xffffff, background_color: 0x1a69a4 } as IColor],
            ])
        ],
        [
            Color.COLOR_SCHEMA_YELLOW,
            new Map([
                [States.STATE_COMMON, { color: 0xffffff, background_color: 0xfbbd08 } as IColor],
                [States.STATE_HOVERED, { color: 0xffffff, background_color: 0xeaae00 } as IColor],
                [States.STATE_PRESSED, { color: 0xffffff, background_color: 0xcd9903 } as IColor],
                [States.STATE_DISABLED, { color: 0xffffff, background_color: 0xfde191 } as IColor],
                [States.STATE_CHECKED, { color: 0xffffff, background_color: 0xcd9903 } as IColor],
            ])
        ],
        [
            Color.COLOR_SCHEMA_OLIVE,
            new Map([
                [States.STATE_COMMON, { color: 0xffffff, background_color: 0xb5cc18 } as IColor],
                [States.STATE_HOVERED, { color: 0xffffff, background_color: 0xa7bd0d } as IColor],
                [States.STATE_PRESSED, { color: 0xffffff, background_color: 0x8d9e13 } as IColor],
                [States.STATE_DISABLED, { color: 0xffffff, background_color: 0xdee898 } as IColor],
                [States.STATE_CHECKED, { color: 0xffffff, background_color: 0x8d9e13 } as IColor],
            ])
        ],
        [
            Color.COLOR_SCHEMA_GREY_BASIC,
            new Map([
                [States.STATE_COMMON, { color: 0x5a5a5a, background_color: 0xffffff, border_color: 0xe0e1e2 } as IColor],
                [States.STATE_HOVERED, { color: 0x282929, background_color: 0xffffff, border_color: 0xcacbcd } as IColor],
                [States.STATE_PRESSED, { color: 0x121212, background_color: 0xffffff, border_color: 0xbabbbc } as IColor],
                [States.STATE_DISABLED, { color: 0xb5b5b5, background_color: 0xffffff, border_color: 0xf1f1f2 } as IColor],
                [States.STATE_CHECKED, { color: 0x121212, background_color: 0xbabbbc, border_color: 0xbabbbc } as IColor]
            ])
        ]
    ]);

    static numberToCssColor(v: number): string {
        return (v < 0) ? "transparent" : '#' + ('00000' + (v | 0).toString(16)).substr(-6);
    }

}
