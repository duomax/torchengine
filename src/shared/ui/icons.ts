export class Icons {
    public static readonly ICON_TRASH: string = "trash";
    public static readonly ICON_TRASH_OPEN: string = "trash-open";
}