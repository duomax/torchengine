import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { WindowService } from "../../../../core/window.service";

@Component({
    selector: 'torch-custom-collapsible-group',
    templateUrl: './custom-collapsible-group.component.html',
    styleUrls: ['./custom-collapsible-group.component.css']
})
export class CustomCollapsibleGroupComponent implements OnInit, AfterViewInit {
    private _open: boolean;
    @Input() get open(): boolean {
        return this._open;
    }
    @Output() openChange: EventEmitter<boolean>;
    set open(v: boolean) {
        this._open = v;
        this.openChange.emit(v);
    }

    torchclick: EventEmitter<Event>;
    unsubscribers: (() => void)[];

    constructor(private elementRef: ElementRef, private windowService: WindowService) {
        this._open = true;
        this.unsubscribers = [];
        this.torchclick = new EventEmitter();
        this.openChange = new EventEmitter();
    }

    ngOnInit() {}

    ngAfterViewInit() {
        this.unsubscribers.push(this.windowService.addEventListener(WindowService.WINDOW_EVENT_CLICK, (event: Event) => {
            if (this.elementRef.nativeElement.querySelector(".collapse-button").contains(event.target)) {
                this.open = !this.open;

                return true;
            }

            return false;
        }));
    }
}
