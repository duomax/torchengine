import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomCollapsibleGroupComponent } from './custom-collapsible-group.component';

describe('CustomCollapsibleGroupComponent', () => {
  let component: CustomCollapsibleGroupComponent;
  let fixture: ComponentFixture<CustomCollapsibleGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomCollapsibleGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomCollapsibleGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
