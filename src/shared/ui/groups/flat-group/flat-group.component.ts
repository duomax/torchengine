import {AfterContentInit, AfterViewInit, Component, ElementRef, Input, OnInit} from '@angular/core';
import {Color, IColor} from "../../colors";
import {Layouts} from "../../../layout/layouts";
import {ButtonState} from "../../buttons/buttons";
import {States} from "../../states";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
    selector: 'torch-flat-group',
    templateUrl: './flat-group.component.html',
    styleUrls: ['./flat-group.component.css']
})
export class FlatGroupComponent implements OnInit, AfterViewInit {
    @Input() caption: string;
    @Input() controls: any[];
    @Input() borderRadius: number;
    @Input() colors: Map<string, IColor>;
    defaultColorSchema: Map<string, IColor>;

    constructor(public layouts: Layouts, private elementRef: ElementRef, private sanitizer: DomSanitizer) {
        this.caption = "";
        this.controls = [];
        this.borderRadius = 5;

        this.colors = new Map();
        this.defaultColorSchema = Color.COLOR_SCHEMAS.get(Color.COLOR_SCHEMA_GREY_BASIC);
    }

    ngOnInit() {
        this.defaultColorSchema.forEach((value: IColor, key: string) => {
            if (!this.colors.get(key)) this.colors.set(key, value);
        });

        // TODO: to torch-icon-button
        if (this.controls.length > 0) {
            this.controls.forEach((control) => {
                control.views.forEach((view: any) => {
                    view.path = this.sanitizer.bypassSecurityTrustResourceUrl("./../assets/icons/" + view.name + ".svg")
                })
            })
        }
    }

    ngAfterViewInit() {
        if (this.controls.length > 0) { // TODO: create torch-icon-button and extract this
            let icons: NodeList = this.elementRef.nativeElement.querySelectorAll(".img-btn");
            let hoveredView: HTMLBaseElement = this.elementRef.nativeElement.querySelector(".hovered");
            Array.prototype.slice.call(icons).forEach((icon: HTMLIFrameElement, i) => {
                icon.onload = () => {
                    let svgDocument: HTMLDocument = icon.contentDocument;
                    if (!svgDocument) {
                        console.error("FlatGroupComponent: Object svg document not found");
                        return;
                    }

                    let svg: SVGSVGElement = svgDocument.querySelector("svg");
                    if (!svg) {
                        console.error("FlatGroupComponent: Wrong svg icon");
                        return;
                    }

                    // Sets color of svg icon
                    svg.style.fill = this.getIconColor();

                    // Shows svg icon after it loaded
                    if (icon.className.indexOf("hovered-icon") >= 0)
                        hoveredView.style.visibility = 'visible';
                }
            });
        }
    }

    onStateChange(event) {
        if (!event.hovered) {
            let hoveredView: HTMLBaseElement = this.elementRef.nativeElement.querySelector(".hovered");
            if (hoveredView)
                hoveredView.style.visibility = 'hidden'
        }
    }

    getCaptionColor(): string {
        return Color.numberToCssColor(this.colors.get(States.STATE_COMMON).border_color);
    }

    getIconColor(): string {
        return Color.numberToCssColor(this.colors.get(States.STATE_COMMON).color);
    }

    getCommonIconPath(i) {
        // return "./../assets/icons/trash.svg"
        return this.controls[i].views.find(view => view.state == States.STATE_COMMON).path;
    }

    getHoveredIconPath(i) {
        return this.controls[i].views.find(view => view.state == States.STATE_HOVERED).path;
        // return this.sanitizer.bypassSecurityTrustResourceUrl("./../assets/icons/" + names.get(States.STATE_HOVERED) + ".svg");
    }
}
