import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlatGroupComponent } from './flat-group.component';

describe('FlatGroupComponent', () => {
  let component: FlatGroupComponent;
  let fixture: ComponentFixture<FlatGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlatGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlatGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
