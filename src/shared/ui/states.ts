export interface IDisabled {
    disabled: boolean
}

export interface IHovered {
    hovered: boolean
}

export interface  IPressed {
    pressed: boolean
}

export type IState = IHovered | IPressed | IDisabled;

export interface IStates {
    readonly _states: IState[]
}

export class States {
    public static readonly STATE_COMMON: string = "common";
    public static readonly STATE_HOVERED: string = "hovered";
    public static readonly STATE_PRESSED: string = "pressed";
    public static readonly STATE_DISABLED: string = "disabled";
    public static readonly STATE_CHECKED: string = "checked";
}
