import {
    AfterContentInit, AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output,
    SimpleChanges
} from '@angular/core';
import { WindowService } from "../../../../core/window.service";

@Component({
    selector: 'app-custom-combobox',
    templateUrl: './custom-combobox.component.html',
    styleUrls: ['./custom-combobox.component.css']
})
export class CustomComboboxComponent implements OnInit, AfterContentInit, AfterViewInit {
    private _items: string[];
    @Input() get items(): string[] {
        return this._items;
    };
    set items(v: string[]) {
        this._items = v;
    }

    @Output() selectedChange: EventEmitter<number>/* = new EventEmitter()*/;
    @Output() selectedValueChange: EventEmitter<string>/*  = new EventEmitter()*/;

    private _selectedValue: string;
    @Input() get selectedValue(): string {
        return this._selectedValue;
    }
    set selectedValue(v: string) {
        this._selectedValue = (!v || v.length == 0) ? this._items[this._selected] : v;
        this.selectedValueChange.emit(this._selectedValue);
    }

    private _selected: number;
    @Input() get selected(): number {
        return this._selected;
    }
    set selected(v: number) {
        // if (v == null || typeof v == "undefined")
        //     return;

        this._selected = v;
        this.selectedChange.emit(this._selected);
    }

    @Output() onchange: EventEmitter<null>;

    private _isOpen: boolean;
    get isOpen(): boolean {
        return this._isOpen;
    }
    set isOpen(v: boolean) {
        this._isOpen = v;
    }
    // isOpen: boolean;

    constructor(private element: ElementRef, public windowService: WindowService) {
        this._items = [];
        this.selectedChange = new EventEmitter();
        this.selectedValueChange = new EventEmitter();
        this.onchange = new EventEmitter();
        this._selected = 0;
        this._isOpen = true;

        this.windowService.addEventListener(WindowService.WINDOW_EVENT_CLICK, (event) => {
            this.toggle(event);
        });
    }

    ngOnInit() {
        this.select(this._selected);
        setTimeout(() => {
            console.log(this.element.nativeElement.querySelectorAll("div.list-content .item"));
            let items = this.element.nativeElement.querySelectorAll("div.list-content .item");
            items.forEach((item, i) => {
                this.windowService.addEventListener(WindowService.WINDOW_EVENT_CLICK, (event) => {
                    if (item.contains(event.target))
                        this.select(i);
                });
                // item.addEventListener("click", () => {
                //     console.log("!!!");
                //     this.select(i);
                // })
                // item.nativeElement.querySelector(".hovered").style.display = "block";
                // item.addEventListener("mouseover", (event) => {
                //     console.log(item.innerText);
                //     item.nativeElement.querySelector(".hovered")
                // });
                // item.addEventListener("mouseleave", (event) => {
                //     console.log(item.innerText);
                //     item.style.color = "black";
                // })
            });
            this.isOpen = false;
        }, 0);
    }

    ngAfterViewInit() {
        // console.log(this.element.nativeElement.querySelector("div.list-content .item"));

    }

    toggle(event): void {
        if (this.element.nativeElement.contains(event.target))
            this.isOpen = !this.isOpen;
        else
            this.isOpen = false;

        // setTimeout(() => {
        //     console.log(this.element.nativeElement.querySelectorAll("div.list-content .item"));
        //     let items = this.element.nativeElement.querySelectorAll("div.list-content .item");
        //     items.forEach((item, i) => {
        //         this.windowService.addEventListener(WindowService.WINDOW_EVENT_CLICK, (event) => {
        //             if (item.contains(event.target))
        //                 this.select(i);
        //         });
        //         // item.addEventListener("click", () => {
        //         //     console.log("!!!");
        //         //     this.select(i);
        //         // })
        //         // item.nativeElement.querySelector(".hovered").style.display = "block";
        //         // item.addEventListener("mouseover", (event) => {
        //         //     console.log(item.innerText);
        //         //     item.nativeElement.querySelector(".hovered")
        //         // });
        //         // item.addEventListener("mouseleave", (event) => {
        //         //     console.log(item.innerText);
        //         //     item.style.color = "black";
        //         // })
        //     })
        // }, 0);
    }

    show(): string {
        return (this.isOpen) ? "block" : "none";
    }

    select(i): void {
        console.log("ma3ks", i);
        this.selected = i;
        this.selectedValue = this._items[i];
        this.onchange.emit();

        let items;

        // if (this.isOpen)
            items = this.element.nativeElement.querySelectorAll("div.list-content .item");

        setTimeout(() => {
            let content = this.element.nativeElement.querySelector("div.content");
            if (items.length == 0) {
                items = this.element.nativeElement.querySelectorAll("div.list-content .item");

            } else {
                console.log(content.lastChild);
                content.removeChild(content.lastChild);
                console.log(content.lastChild);
            }


            console.log(items);
            content.appendChild(items[this.selected].cloneNode(true));
        }, 0)
    }

    ngOnChanges(changes: SimpleChanges) { // TODO: need or not?
        if (changes.items && changes.items.previousValue && changes.items.previousValue.toString() != changes.items.currentValue.toString()) {
            this.selectedValue = this._items[this._selected];
        }
    }

    ngAfterContentInit() { // TODO: need or not?
        // console.log(this.element.nativeElement.querySelector("div.list-content .item"));
        if (!this._selectedValue && this._items)
            this.selectedValue = this._items[this._selected]
    }

}
