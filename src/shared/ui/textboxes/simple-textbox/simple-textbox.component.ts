import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
    selector: 'torch-simple-textbox',
    templateUrl: './simple-textbox.component.html',
    styleUrls: ['./simple-textbox.component.css']
})
export class SimpleTextboxComponent implements OnInit {
    private _text: string;
    @Output() textChange: EventEmitter<string>;
    @Input() get text(): string {
        return this._text;
    };
    set text(v: string) {
        this._text = v;
        this.textChange.emit(v);
    }

    constructor() {
        this._text = "";
        this.textChange = new EventEmitter();
    }

    ngOnInit() {
    }

}
