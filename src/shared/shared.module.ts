import { NgModule, PLATFORM_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";

// Layouts
import { Layouts } from "./layout/layouts";
import { ColsComponent } from "./layout/cols/cols.component";
import { RowsComponent } from "./layout/rows/rows.component";

// Base Components
import { InteractiveComponent } from './ui/interactive/interactive.component';

// Custom Components
import { CustomButtonComponent } from './ui/buttons/custom-button/custom-button.component';
import { CustomCheckedButtonComponent } from './ui/buttons/custom-checked-button/custom-checked-button.component';
import { CustomListComponent } from "./ui/lists/custom-list/custom-list.component";

// End Components
import { FlatButtonComponent } from "./ui/buttons/flat-button/flat-button.component";
import { SimpleButtonComponent } from './ui/buttons/simple-button/simple-button.component';
import { SimpleTextboxComponent } from './ui/textboxes/simple-textbox/simple-textbox.component';

// Components Parts
import { FlatButtonBasisComponent } from './ui/buttons/flat-button-basis/flat-button-basis.component';

// Services
import { WindowService } from "../core/window.service";

import { ListComponent } from "./ui/lists/list/list.component";
import { ListItemComponent } from "./ui/lists/list-item/list-item.component";
import { FlatListComponent } from './ui/lists/flat-list/flat-list.component';
import { FlatCheckedButtonComponent } from './ui/buttons/flat-checked-button/flat-checked-button.component';
import {ButtonAlign} from "./ui/buttons/buttons";
import { CustomGroupComponent } from './ui/groups/custom-group/custom-group.component';
import { FlatGroupComponent } from './ui/groups/flat-group/flat-group.component';
import {DebugService} from "../core/debug.service";
import {Icons} from "./ui/icons";
import { CustomCollapsibleGroupComponent } from './ui/groups/custom-collapsible-group/custom-collapsible-group.component';

@NgModule({
    imports: [CommonModule, FormsModule],
    declarations: [
        // Layouts
        ColsComponent,
        RowsComponent,

        // Base Components
        InteractiveComponent,

        // Custom Components
        CustomButtonComponent,
        CustomCheckedButtonComponent,
        CustomListComponent,

        // End Components
        FlatButtonComponent,
        FlatCheckedButtonComponent,
        SimpleButtonComponent,
        SimpleTextboxComponent,

        // Components Parts
        FlatButtonBasisComponent,

        ListComponent,
        ListItemComponent,
        FlatListComponent,
        CustomGroupComponent,
        FlatGroupComponent,
        CustomCollapsibleGroupComponent,

    ],
    providers: [ WindowService, Layouts, ButtonAlign, DebugService ],
    exports: [
        FormsModule,
        InteractiveComponent,
        CustomButtonComponent,
        FlatButtonComponent,
        ColsComponent,
        RowsComponent,
        CustomListComponent,
        ListComponent,
        ListItemComponent,
        FlatListComponent,
        CustomCheckedButtonComponent,
        SimpleTextboxComponent,
        SimpleButtonComponent,
        FlatCheckedButtonComponent,
        FlatGroupComponent
    ],
    entryComponents: [FlatCheckedButtonComponent],
})
export class SharedModule { }
