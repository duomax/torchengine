export class Layouts {
    private static readonly _LAYOUTS_START: string = "flex-start";
    private static readonly _LAYOUTS_END: string = "flex-end";
    private static readonly _LAYOUTS_SPACE_BETWEEN: string = "space-between";
    private static readonly _LAYOUTS_SPACE_AROUND: string = "space-around";
    private static readonly _LAYOUTS_CENTER: string = "center";

    get LAYOUTS_START() {
        return Layouts._LAYOUTS_START;
    }

    get LAYOUTS_END() {
        return Layouts._LAYOUTS_END;
    }

    get LAYOUTS_SPACE_BETWEEN() {
        return Layouts._LAYOUTS_SPACE_BETWEEN;
    }

    get LAYOUTS_SPACE_AROUND() {
        return Layouts._LAYOUTS_SPACE_AROUND;
    }

    get LAYOUTS_CENTER() {
        return Layouts._LAYOUTS_CENTER;
    }
}
