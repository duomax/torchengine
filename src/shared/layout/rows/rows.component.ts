import {Component, HostBinding, Input, OnInit} from '@angular/core';
import {Layouts} from "../layouts";

@Component({
    selector: 'torch-rows',
    templateUrl: './rows.component.html',
    styleUrls: ['./rows.component.css']
})
export class RowsComponent implements OnInit {
    @HostBinding('style.justify-content') @Input() justifyItems;
    @HostBinding('style.align-items') @Input() alignItems;

    constructor(private layouts: Layouts) {
        this.justifyItems = this.layouts.LAYOUTS_START;
        this.alignItems = this.layouts.LAYOUTS_START;
    }

    ngOnInit() {
    }

}
