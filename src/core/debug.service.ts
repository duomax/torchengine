import { Injectable } from '@angular/core';

@Injectable()
export class DebugService {
    static warningGroup(name: string, messages: string[]): void {
        if (messages && messages.length > 0) {
            console.group(name);
            messages.forEach((message: string) => console.warn(message));
            console.groupEnd();
        }
    }

    constructor() { }

}
