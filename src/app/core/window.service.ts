import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {isPlatformBrowser} from "@angular/common";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/observable/fromEvent';

@Injectable()
export class WindowService {
    public static readonly WINDOW_EVENT_CLICK: string = "click";
    public static readonly WINDOW_EVENT_RESIZE: string = "resize";
    public static readonly WINDOW_EVENT_MOUSE_ENTER: string = "mouseenter";
    public static readonly WINDOW_EVENT_MOUSE_LEAVE: string = "mouseleave";
    public static readonly WINDOW_EVENT_MOUSE_OVER: string = "mouseover";
    public static readonly WINDOW_EVENT_MOUSE_MOVE: string = "mousemove";
    public static readonly WINDOW_EVENT_MOUSE_DOWN: string = "mousedown";
    public static readonly WINDOW_EVENT_MOUSE_UP: string = "mouseup";

    // Returned boolean value of value function is StopPropagation
    private _windowEvents: Map<string, Array<(event: Event) => boolean>>;

    private _window: any;
    get window(): any {
        return this._window;
    }
    set window(v: any) {
        this._window = v;
    }

    private _subscriptions: Observable<Event>[];

    constructor(@Inject(PLATFORM_ID) private platformId: Object) {
        // Defines window events
        this._windowEvents = new Map([
            [WindowService.WINDOW_EVENT_CLICK, []],
            [WindowService.WINDOW_EVENT_RESIZE, []],
            [WindowService.WINDOW_EVENT_MOUSE_ENTER, []],
            [WindowService.WINDOW_EVENT_MOUSE_LEAVE, []],
            [WindowService.WINDOW_EVENT_MOUSE_OVER, []],
            // [WindowService.WINDOW_EVENT_MOUSE_MOVE, []],
            [WindowService.WINDOW_EVENT_MOUSE_DOWN, []],
            [WindowService.WINDOW_EVENT_MOUSE_UP, []],
        ]);

        if (isPlatformBrowser(this.platformId)) {
            this.window = window;

            this._subscriptions = [];
            this._windowEvents.forEach((value: Array<(event: Event) => boolean>, key: string) => {
                this._subscriptions.push(Observable.fromEvent(this.window, key));
            });
            this._subscriptions.forEach((subscription: Observable<Event>) => {
                subscription.subscribe((event: Event) => {
                    let handlers: Array<(event: Event) => boolean> = this._windowEvents.get(event.type).slice();

                    // if (event.type == "click")
                    //     console.log("ma3ks WS: handlers", handlers);
                    //
                    // If StopPropagation then stop executing functions
                    handlers.some((value: (event: Event) => boolean) => value(event));
                });
            });
        } else {
            this.window = {
                innerWidth: 0,
                scrollTo(x: number, y: number) {}
            }
        }
    }

    // Adds events callback. Returns unsubscribe function
    addEventListener(event: string, currentCallback: (event: Event) => boolean): () => void {
        this._windowEvents.get(event).push(currentCallback);

        return () => {
            let index: number = this._windowEvents.get(event).findIndex((callback: (event: Event) => boolean) => {
                return callback === currentCallback
            });

            if (index >= 0)
                this._windowEvents.get(event).splice(index, 1);
        }
    }
}
